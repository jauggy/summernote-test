/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function() {
    angular.module('inspinia', [
        'ui.router', // Routing
        'ui.bootstrap' // Bootstrap
        , 'summernote' //This line breaks code
    ])
})();
